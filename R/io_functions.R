# Input Output functions

ExtractExt <- function(inputfile) {
  tolower(sub("(.+)\\.(rda|rds|csv|txt|dpt|json|JSON)$","\\2", 
              basename(inputfile)))
}

FillReader <- function(specfile) {
  out <- list(ext = ExtractExt(specfile))
  if (out$ext == "csv") out$header <- TRUE
  out
}

InitFolder <- function(x, loc.time) {
  folder <- file.path(paste(gsub(" ", "_", x$description), loc.time, sep = "_"))
  dir.create(folder, showWarnings = FALSE)
  dir.create(file.path("www", folder), showWarnings = FALSE)
  return(folder)
}

InitOutFolders <- function(tabs) {
  loc.time <- format(Sys.time(), "%Y%m%d_%H%M%S")
  out <- tabs %>% map_chr(., InitFolder, loc.time)
  return(out)
}

InitInputFile <- function(x, data.in, folder, inputfile) {
  casepath <- ifelse(x == "internal", "datapath", "name")
  inputfile$specfile <- unname(data.in[[casepath]])
  inputfile$specfile_reader <- FillReader(inputfile$specfile)
  write(toJSON(inputfile), file.path(folder, paste(names(folder), "-", x, ".json", sep = "")))
}

FillSpecInput <- function(data.in, info) {
  cases <- c("user", "internal")
  for (idx in names(info$folder)) {
    inputfile <- fromJSON(file.path(info$file.loc$inputfiles, 
                                    paste(idx, "input.json", sep = "-")))
    map(cases, InitInputFile, data.in, info$folder[idx], inputfile)
  }
}

UpdateInputFile <- function(inputlist, info) {
  walk2(c("folder", "file.loc", "ns", "id"), info, assign, envir = environment())
  for (idx in c("user", "internal")) {
    infile <- file.path(folder[id], paste(ns(idx), "json", sep = "."))
    if (file.exists(infile)) {
      parm <- FillParm(as.list(fromJSON(infile)), MakeOutList(inputlist, idx))
      write(toJSON(parm), infile)
    }
  }
}

FillParm <- function(args, defaults) {
  for (x in names(defaults)) 
    args[x] <- defaults[x]
  args[sapply(args, is.null)] <- NULL
  args
}

MakeOutList <- function(input, case) {
  outlist <- input
  casepath <- ifelse(case == "internal", "datapath", "name")
  outlist <- map(input, ReplaceName, casepath)
  return(outlist)
}

ReplaceName <- function(x, casepath) {
  expr <- class(x) == "data.frame" && !is.null(x[casepath]) && !is.na(x[casepath])
  x <- if (expr) unname(x[[casepath]]) else x
  return(x)
}

FunName <- function(id, base.fun, name.fun = "TabHandler") 
  assign(paste0(id, name.fun), base.fun, envir = .GlobalEnv)

