#!/usr/bin/Rscript

SetPlot <- function(xlimits, ylimits){
  #  par(mar = c(3, 3, 3, 3), mgp = c(0,.5,0))
  plot.new()
  plot.window(xlim = xlimits, ylim = ylimits)
  axis(1, tck = 0.025) 
  axis(2, tck = 0.025) 
  axis(3, tck = 0.025, labels = FALSE)
  axis(4, tck = 0.025, labels = FALSE) 
  box()
}

outfile <- "results.pdf"

options(stringsAsFactors = FALSE)

library(RJSONIO)
library(APRLspec)
library(tidyverse)
library(grDevices)

## parse arguments
argv <- ParseArgs()
userinput <- argv$args[1]
runpath <- dirname(argv$args[1])

## read files
parm <- as.list(fromJSON(userinput))
spec <- ReadSpec(parm$specfile)
responses <- read.csv(parm$responsefile, header = TRUE)

samplesel <- responses$sample

## subset spec file
x1 <- Wavenumbers(spec)
y1 <- spec[samplesel[seq(1, 10, by = 1)], ]

## plot and save results
pdf(file.path(runpath, outfile), width = 6, height = 4)
SetPlot(xlimits = c(4000, 500), ylimits = range(y1))
matlines(x1, t(y1), lwd = 2, lty = 1, col = rgb(t(col2rgb("blue") / 255), alpha = 0.3))
title(xlab = expression(paste("Wavenumber (cm" ^ "-1",")")),
      ylab = "Absorbance")
dev.off()
