<!DOCTYPE html>

<h2> Calibration</h2> 
<p>This tab implements the multivariate regression and calibration using aerosol FT-IR spectra described in the following manuscripts:
  <a href="http://dx.doi.org/10.5194/amt-8-1097-2015" target="_blank">Dillner, and Takahama, Atmos. Meas. Tech., 2015a</a>,
  <a href="http://dx.doi.org/10.5194/amt-8-4013-2015" target="_blank">Dillner, and Takahama, Atmos. Meas. Tech., 2015b</a> and
  <a href="http://dx.doi.org/10.5194/amt-9-441-2016" target="_blank">Reggente, Dillner, and Takahama, Atmos. Meas. Tech., 2016</a>. Currently implemented for PLS1 and PLS2 using the <a href="https://cran.r-project.org/web/packages/pls/index.html" target="_blank">pls package</a> <a href="http://dx.doi.org/10.18637/jss.v018.i02" target="_blank"> (Mevik and Wehrens, J. Stat. Softw., 2007)</a>.

<h3>Inputs before computation:</h3>

<strong>Response file (y)</strong> 
<p>File containing the response values (target variables).</p>

<strong>Case file</strong>
<p>File containing the list of samples to be used in the calibration and test.</p>

<strong>Variable(s)</strong>
<p>The User can choose the target variable(s) from a list given by the column names of the response file uploaded. Multiple choices are allowed. If the PLS type parameter is set to PLS1 (default), the AIRSpec will compute one calibration model for each variable. If the PLS type parameter is set to PLS2, the AIRSpec will compute one calibration model for whole variables matrix.</p>

<strong>Spectra type</strong>
<p>The user can choose which spectra to use: the uploaded spectra (Home tab) or the baseline corrected spectra (Baseline correction tab). </p>

<strong>Show optional inputs (optional) checkbox:</strong>
<ul>
  <li><strong>Wavenumber:</strong> the user can upload a file containing indices for selecting wavenumber (this file should contain integers or list of integers corresponding to each variable). For example, the index 1 corresponds to the most left wavenumber. If the user does not upload a file, as default, AIRSpec uses all wavenumbers.</li>
  <li><strong>Minimum detection limit:</strong> the user can upload a file containing the minimum detection limit (MDL) for each variable. AIRSpec will remove samples below the MDL.</li>
</ul>

<strong>Show/change default parameters checkbox:</strong>
<ul>
  <li><strong>Max number of PLS components:</strong> the user can choose the upper limit of the number of latent variables (default is 60).</li>
  
  <li><strong>Number of segments for CV:</strong> the user can choose the number of folders (segments) in the k-fold cross-validation (default is 10). Please, be aware that the maximum number of segments has to be equal or minor the number of samples in the calibration data set.</li>
  
  <li><strong>PLS Method:</strong> the user can choose the fitting algorithm (default is the orthogonal scores algorithm, oscorespls).</li>
  
  <li><strong>PLS type</strong> the user can choose between PLS1 (default) and PLS2. In the case of PLS1, the AIRSpec will compute one calibration model for each variable. In the case of PLS2, the AIRSpec will compute one calibration model for whole variables matrix (the number of variables needs to be higher than one).</li>
  
  <li><strong>Parameters optimization</strong> the user can choose between cross-validation (CV, default), leave-one-out (LOO). In the case of <strong>CV</strong>, k-fold cross-validation is performed. The number and type of cross-validation segments are specified in the fields <strong>Number of segments for CV</strong> and <strong>Segment type</strong> (see below). In the case of <strong>LOO</strong>, leave-one-out cross-validation is performed. In this case, the input <strong>Number of segments for CV</strong> is ignored. In these two cases (cross-validation is used), the calibration set is used for both "training" and "validation," and the test set is completely separate from the model building process. Differently, in the case of <strong>none</strong>, the test set is used for "validation." Be aware that in this case, the number of components (LVs) is chosen according to the test set.</li>
  
  <li><strong>Segment type</strong> the user can choose how to generate the CV segments (folds).
  <strong>Interleaved</strong> is the default, and the first segment will contain the indices <i>1, length.seg+1, 2*length.seg+1, …, (k-1)*length.seg+1</i>, and so on. <i>lenght.seg</i> is the ceiling of the number of calibration samples divided by the number of segments (k). In the case of <strong>random</strong>, the indices are allocated to segments in random order. In the case of <strong>consecutive</strong>, the first segment will contain the first length.seg indices, and so on.</li>
  
  <li><strong>order response variable:</strong> the user can choose to order the calibration samples according to the concentrations. </li>
  
  <li><strong>Figure units:</strong> micrograms, micromoles or micrograms per cubic meter.</li>
</ul> 

<strong>Compute</strong>
<p>Click the Compute button to start the computation, with the setting described above. At the end of the calculation, the AIRSpec produce a figure with different plots. Each row corresponds to a variable. From left to right, in the first panel, there is the RMSE in cross-validation against the number of components (latent variables), the dotted vertical line shows the number of components selected according to the minimum RMSE. The second and third panels show the scatter plots of predicted against observed (or reference) values for the calibration and test datasets.</p>


<h3>Inputs after computation:</h3>

<strong>Download results</strong>
<p>Click to download the following files:</p>
<ul>
  <li>
    <strong>fits.rds</strong> file containing the fitted model for each variable. The file contains all the model specifications (e.g., regression coefficients, scores, loadings, loading weights, fitted values, residuals, and so on).
  </li>
  <li>
    <strong>fitsplot.pdf</strong> Figure with different plots. Each row corresponds to a variable. From left to right, in the first panel, there is the RMSE in cross-validation against the number of components (latent variables), the dotted vertical line shows the number of components selected according to the minimum RMSE. The second and third panels show the scatter plots of predicted against observed (or reference) values for the calibration and test sets.
  </li>
  <li>
    <strong>input_mvr_user.json</strong> For the sake of reproducibility, the file contains all the input parameters used in the computation of the calibration model. 
    <ul>
      <li><strong>param:</strong> list of pls parameters:</li>
      <ul>
        <li><strong>method:</strong> pls fitting algorithm (see <strong>PLS Method</strong> above).</li>
        <li><strong>ncomp:</strong> upper limit of the number of latent variables (see <strong>Max number of PLS components</strong> above)</li>
        <li><strong>validation:</strong> method for <strong>Parameters optimization</strong>, see above.</li>
        <li><strong>segments:</strong> number of folders (segments) used in the cross-validation (see <strong>Number of segments for CV</strong> above).</li>
        <li><strong>segment.type:</strong> method to generate the CV segments (see <strong>Segment type</strong> above.</li>
      </ul>
      <li><strong>responsefile:</strong> name of the file that contains the list of the samples with the variable concentrations.</li>
      
      <li><strong>plstype:</strong> type of the pls used (see <strong>PLS type</strong> above).</li>
      <li><strong>variables:</strong> list of variable computed (see <strong>Variable(s)</strong> above).</li>
      <li><strong>casefile:</strong> name of the file that contains the list of the samples used for calibration and test.</li>
      <li><strong>specfile:</strong> name of the file that contains the spectra matrix used (see <strong>Spectra type</strong> above).</li>
      <li><strong>mdlfile:</strong> name of the file that contains the MDL values (see <strong>Minimum detection limit</strong> above).</li>
      
      <li><strong>wavenumfile:</strong> name of the file that contains the indices of the selected wavenumber (see <strong>Wavenumber</strong> above).</li>
      <li><strong>reordercalib:</strong> true if the calibration samples are reordered according to the concentrations (see <strong>order response variable</strong> above).</li>
      <li><strong>units:</strong> name of the units used in the figures (see <strong>Figure units</strong> above).</li>
      <li><strong>responsefile_reader:</strong> responsefile type.</li>
      <li><strong>specfile_reader:</strong> specfile type.</li>
      <li><strong>casefile_reader:</strong> casefile type.</li>
    </ul>
  </li>
  <li>
    <strong>pred.rda:</strong> file containing the predicted values for each variable and the whole set of components. 
  </li>
  <li>
    <strong>prediction_table.csv</strong> file containing the table with the predicted and observed values (for each variable) for the optimal number of components (LVs).
  </li>
  <li>
    <strong>rmsep.rda</strong> file containing the RMSE values obtained in cross-validation.
  </li>
  <li>
    <strong>stats_table.csv</strong> file containing the statistics of the fitted models.
  </li>
  <li>
    <strong>[variable]_VIPscores.pdf</strong> file containing the plot of the Variance Importance in Projection (VIP) scores as described in <a href="http://dx.doi.org/10.1016/j.chemolab.2004.12.011" target="_blank"> Chong, Il-Gyo and Jun, Chi-Hyuck, Chemometr. Intell. Lab., 2005</a> for each variable.
  </li>
  <li>
    <strong>[variable]_EVx.pdf</strong> file containing the plot of the explained variation in X (<var>EV</var><sub><var>jk</var></sub><sup><var>(X)</var></sup>) for each LV.
  </li>
  <li>
    <strong>[variable]_EVy.pdf</strong> file containing the plot of the explained variation in Y (<var>EV</var><sub><var>jk</var></sub><sup><var>(Y)</var></sup>) for each LV.
  </li>
  <li>
    <strong>[variable]__coefficients.pdf</strong> file containing the plot of the regression coefficients.
  </li>
</ul>

      