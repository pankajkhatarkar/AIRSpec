
library(AIRSpec)
library(Rfunctools)
library(RSQLite)
library(RJSONIO)
library(APRLspec)
library(APRLssb)
library(APRLmpf)
library(APRLmvr)
library(tidyverse)
library(shinythemes)
library(purrr)
library(shiny)
library(shinyjs)
library(pls)
library(grDevices)

file.loc <- as.list(fromJSON(file.path("inputfiles", "file_loc.json")))
map(names(fromJSON(file.loc$tablist)), FunName, TabHandler)

shinyUI(
  fluidPage(theme = shinytheme("flatly"),
            #tags$head(includeScript("google-analytics.js")),
            #tags$header(includeScript("epfl-cookie-consent.js")),
            titlePanel(title = tags$div(
              tags$img(src = "EPFL_Logo_Digital_RGB_PROD.jpg", 
                       heigth = 120, width = 120, class = "pull-left"),
              tags$h1("AIRSpec - Tools for Aerosol InfraRed Spectroscopy", 
                      align = "center")),
              windowTitle = "AIRSpec"),
            navbarPage(title = "",
                       #########################################################
                       ### Home tab
                       homeTabUI(id = "home", file.loc, tabname = "Home"),
                       #########################################################
                       ### Baseline correction tab
                       ssbTabUI(id = "ssb", tabname = "Baseline correction"),
                       # #######################################################
                       # ### Peak fitting tab
                       mpfTabUI(id = "mpf", tabname = "Peak fitting", file.loc),
                       # #######################################################
                       # ### Calibration tab
                       calTabUI(id = "cal", tabname = "Calibration") #, 
                       # #######################################################
                       # ### To add a tab remove the "#" before "," above.
                       # ### My tab
                       # myTabUI(id = "my", tabname = "My new tab)
                       # #######################################################
            ) # navbarPage
  ) # fluidPage
) # shinyUI







