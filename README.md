R package: AIRSpec
===

This package contains the Shiny fiunctions and module needed to install on a local machine the AIRSpec.
The AIRSPec is is an interactive grafical interface (accesable by a broswer at [AIRSpec](http://airspec.epfl.ch/)) for using the chemometric tools that we tha APRL laboratory at EPFL has developed for FT-IR spectra processing and analysis of atmospheric aerosol. 

The available chemometric tools are accessible by clicking on the navigation tabs present in the graphical interface:  
- **Baseline correction** implements the smoothing splines baseline correction method of [Kuzmiakova, Dillner, and Takahama, Atmos. Meas. Tech., 2016](http://dx.doi.org/10.5194/amt-9-2615-2016).  
- **Peak fitting** implements the multi-peak fitting algorithm of [Takahama, Johnson, and Russell, Aerosol Sci. Tech., 2013](http://dx.doi.org/10.1080/02786826.2012.752065), with additional modifications for extendibility.  
- **Calibration** consolidates functionality for multivariate regression and calibration using aerosol FT-IR spectra described in the following manuscripts: [Dillner, and Takahama, Atmos. Meas. Tech., 2015a](http://dx.doi.org/10.5194/amt-8-1097-2015), [Dillner, and Takahama, Atmos. Meas. Tech., 2015b](http://dx.doi.org/10.5194/amt-8-4013-2015) and [Reggente, Dillner, and Takahama, Atmos. Meas. Tech., 2016](http://dx.doi.org/10.5194/amt-9-441-2016).  

The AIRSpec is academically developed and managed by [Dr. Matteo Reggente](https://people.epfl.ch/matteo.reggente?lang=en) and [Prof. Satoshi Takahama](https://people.epfl.ch/satoshi.takahama?lang=en). If you use results or developments from these chemometric tools (either via the web interface or this package), please cite the manuscripts above. A manuscript documenting this software suite is submitted to [Atmospheric Measurement Technique (AMT)](https://www.atmospheric-measurement-techniques.net/).

Subject to substantial modification. Keep up-to-date with `git pull`.

## Installation

The AIRSpec requires the following R packages available at the ["The Comprehensive R Archive Network"](https://cran.r-project.org/):

- RSQLite
- RJSONIO
- tidyverse
- shinythemes
- purrr
- shiny
- pls
- grDevices
- envDocument

The user can istall the AIRSpec by typing in the command line:

```{sh}
$ R -e "install.packages('PACKAGE_NAME')"
```
Alternatevely, you can open an R session and run
```{r}
install.packages("PACKAGE_NAME")
```
For the packages:
- Rfunctools
- APRLspec
- APRLssb
- APRLmpf
- APRLmvr
please see [installation instructions for  APRLspec](https://gitlab.com/aprl/APRLspec), [APRLssb](https://gitlab.com/aprl/APRLssb), [APRLmpf](https://gitlab.com/aprl/APRLmpf) and [APRLmvr](https://gitlab.com/aprl/APRLmvr), on which AIRSpec depends.

Install for R without installing the dependencies (required packages). replace `{username}` and `{password}` with account for which access to private repository is granted:

```{r}
devtools::install_git('https://{username}:{password}@gitlab.com/aprl/AIRSpec.git')
```

Install for R installing the dependencies (required packages):
```{r}
devtools::install_git('https://{username}:{password}@gitlab.com/aprl/AIRSpec.git', dependencies = TRUE)
```

Alternatively the user can run the script in the section [Installation script](#installation-script)

After installation, get a copy of the Shiny functions and module. To copy them into a new folder called "Folder_Name" in your working directory:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('AIRSpec', 'ShinyApp', path = 'Folder_Name', recursive = TRUE)"
$ cd Folder_Name/
```
To copy them into an existing working directory use "." as third argument:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('AIRSpec', 'ShinyApp', path = '.', recursive = TRUE)"
```

To uninstall, run the command in R:

```{r}
remove.packages("AIRSpec")
```

### Installation script:
The following R scripts installs the **AIRSpec**, **Rfunctools**, **APRLspec**, **APRLmpf**, **APRLmvr** packages and the dependecies.

[Install](http://stackoverflow.com/a/27321143/143476) these packages in R (replace `{username}` and `{password}` with account for which access to private repository is granted):

```{r}
install.packages("devtools")

library(devtools)

pkglist <- c("AIRSpec", "Rfunctools", "APRLspec", "APRLmpf", "APRLmvr", "APRLssb")
username <- '{username}'
password <- '{password}'

for (pkg in pkglist) {
  print(paste("Installing package:", pkg, "..."))
  
  ## install package
  devtools::install_git(sprintf('https://%s:%s@gitlab.com/aprl/%s.git', 
                                username, password, pkg))
                                
  ## install dependencies
  descr <- readLines(file.path(.libPaths(), pkg, "DESCRIPTION"))
  deps <- strsplit(sub("^Imports: ", "", grep("^Imports: ", descr, value = TRUE)), ", ")
  if (exist("deps$1"))  install.packages(deps[[1]])
}  
```

[Alternatively](http://stackoverflow.com/a/41645168/143476), if you have your RSA keys generated on your machine:

```{r}
install.packages("devtools")
install.packages("git2r")

library(devtools)
library(git2r)

creds <- git2r::cred_ssh_key("~/.ssh/id_rsa.pub", "~/.ssh/id_rsa")

pkglist <- c("AIRSpec", "Rfunctools", "APRLspec", "APRLmpf", "APRLmvr", "APRLssb")

for(pkg in pkglist) {
   print(paste("Installing package:", pkg, "..."))

   ## install package
   devtools::install_git(sprintf(git@gitlab.com:aprl/%s.git", pkg), 
                         credentials = creds)

   ## install dependencies
   descr <- readLines(file.path(.libPaths(), pkg, "DESCRIPTION"))
   deps <- strsplit(sub("^Imports: ", "", grep("^Imports: ", descr, 
                    value=TRUE)), ", ")
   if (exist("deps$1"))  install.packages(deps[[1]])
}
```

#### RSQLite/RDB

**Note**: The RSQLite package should be modified as described for [APRLspec's README.md](https://gitlab.com/aprl/APRLspec#rsqliterdb).

Number of maximum allowed variables must be modified. Clone https://github.com/rstats-db/RSQLite/ and edit src/vendor/sqlite3/sqlite3.c.

Change

```{c}
# define SQLITE_MAX_VARIABLE_NUMBER 999
```

to

```{c}
# define SQLITE_MAX_VARIABLE_NUMBER 5000
```

or desired number. Note: in contrast to previous claim,`SQLITE_MAX_COLUMN` (default 2000) does not have to be changed.

Then, in R:

```{r}
setwd("/path/to/dir/") # assuming repo is at /path/to/dir/RSQLite
devtools::install("RSQLite")
```

Alternatively, to perform these tasks in R:

```{r}
reponame <- "git@github.com:r-dbi/RSQLite.git"
filename <- "RSQLite/src/vendor/sqlite3/sqlite3.c"
change <- list(from="# define SQLITE_MAX_VARIABLE_NUMBER 999",
               to="# define SQLITE_MAX_VARIABLE_NUMBER 5000")

## clone repository
system(paste("git clone", reponame))

## modify file
lines <- readLines(filename)
ix <- grepl(change$from, lines, fixed=TRUE)
lines[ix] <- change$to
writeLines(lines, filename)

## install
devtools::install("RSQLite")
```



## Run the AIRSpec
The user can run the AIRSpec by typing in the command line:

```{sh}
$ cd /path/to/workingdir/
$ Rscript RunApp.R
```
To stop the AIRSpec press Ctrl+C.

## Home tab
The first page that the user encounters is the homepage, in which the user can read necessary information and download the template files required as inputs for the chemometric packages. In the homepage, the AIRSpec requires that the user uploads the file with the spectra (a warning is present if the user does not upload the file with the spectra), and once is uploaded it plots a maximum of 100 spectra lines. At this stage, the user can proceed to the available chemometric packages by clicking on the navigation tabs. In the home tab the user can download the template files for the file inputs in each tab.

## Baseline correction
In the baseline correction tab, the user can upload an optional file containing the list of samples to process. If the user does not upload the file, the AIRSpec will baseline correct all the samples present in the uploaded spectra file. Moreover, the user can also choose the desired sequence of EDF values for which different baselines are computed. The user can also choose to use or not a non-negativity constraint. In the the former case, an alternative function will be used to avoid baseline corrected negative absorbances. By clicking the Compute button, the AIRSpec will execute the baseline source code passing as input a file with the desired configuration. At the end of the computation, the AIRSpec plots the spectra of the baseline-corrected samples using the EDF parameter which minimize the median of the NAF (as reported [Kuzmiakova, Dillner, and Takahama, Atmos. Meas. Tech., 2016](http://dx.doi.org/10.5194/amt-9-2615-2016)). If wanted, the user can change the EDF value (the plot will automatically update). The results can be downloaded by clicking the download button.
 
## Peak fitting
In the peak-fitting tab, the user can choose to use the uploaded spectra matrix file (uploaded in home tab) or, if already computed, the baseline corrected spectra matrix (output of the baseline correction tab). Moreover, the user can choose the sequence of FGs (default provided) to be fitted, and the samples to process (as default the AIRSpec will peak-fit all the samples present in the spectra matrix). By clicking the Compute button, the AIRSpec will execute the peak-fitting source code passing as input a file with the desired configuration. At the end of the computation, the AIRSpec plots each spectrum with the fitted peaks, and bar plots containing the FG distribution in organic mass (OM) and the OM/OC ratio (for each sample and the mean values of the all sample computed). The results can be downloaded by clicking the download button.

## Multivariate calibration
In the calibration tab, the user can choose to use the uploaded spectra matrix file (uploaded in home tab) or, if already computed, the baseline corrected spectra matrix (output of the baseline correction tab). Differently, to the baseline correction and peak-fitting packages, the user is required to provide two files: one containing the response values (target variables), and one containing the list of samples to be used for calibration and test. After providing the response file, the user can choose the target variables of the regression from a list given by the column names of the response file uploaded.
The user can choose a single or multiple variables, and accordingly to the type of PLS desired (PLS type field), in the case of multiple variables the AIRSpec will process a regression model for each variable (PLS1) or a regression model for the whole matrix of variables (PLS2). The user can also upload an optional file to use only specified wavenumbers, or to exclude responses below the minimum detection limit of a measurement device. Moreover, the user can change default parameters used in the regression (e.g. fitting algorithm, parameters optimization criteria, limit the number of latent variables). By clicking the
Compute button, the AIRSpec will execute the calibration source code passing as input, a file with the desired configuration. At the end of the computation, the AIRSpec, for each target variable, plots the RMSE in cross-validation against the number of components (latent variables), scatter plots of predicted against observed (or reference) values for the calibration and test datasets. The results can be downloaded by clicking the download button.

## How to add modules/tabs
To add a new module or tab please follow the instructions present in the pdf file addTab_demo/AIRSpec_architecture_and_new_modules.pdf.


