<!DOCTYPE html>

<h2> Baseline correction</h2> 
<p>This tab implements the smoothing splines baseline correction method of 
<a href="http://dx.doi.org/10.5194/amt-9-2615-2016" target="_blank">Kuzmiakova, Dillner, and Takahama, Atmos. Meas. Tech., 2016</a>.
</p>

<h3>Inputs before computation:</h3>

<strong>Sample list file (optional)</strong>
<p>List of samples to be processed. If not uploaded the AIRSpec will baseline correct all the samples present in the spectra file uploaded.</p>
  
<strong>Sequence of EDF values</strong>
<p>The AIRSpec will compute the baseline for each EDF value listed in this field and for each segment selected (see below).</p>

<strong>Checkbox Segment 1 (4000 &mdash; 1820 cm<sup>-1</sup>) and Segment 2 (2000 &mdash; 1425 cm<sup>-1</sup>)</strong>
<p>Checkbox to select which portion of the spectra will be processed (see in the plot the portion of the spectra selected &mdash; red: Segment 1, blue: Segment 2).</p>

<strong>Compute</strong>
<p>Click the Compute button to start the computation, with the setting described above. At the end of the calculation, the AIRSpec plots the spectra of the baseline-corrected samples with default parameters (see below).</p>

<h3>Inputs after computation:</h3>

<strong>Segment 1 EDF and Segment 2 EDF</strong>
<p>Desired EDF values for the baseline-corrected spectra. The spectra with the selected EDF values are plotted on the right. For each segment, the default value is set to be the minimum median of the negative absorbance fraction (NAF &mdash; for details see Section 2.3.2 in <a href="http://dx.doi.org/10.5194/amt-9-2615-2016" target="_blank">Kuzmiakova, Dillner, and Takahama, Atmos. Meas. Tech., 2016</a>) computed from all the samples baseline-corrected and for each EDF.</p>

<strong>Stitch method</strong>
<p>Method to join the baseline-corrected segments together in the overlapping background regions between 2000 and 1820 cm<sup>-1</sup>. The background regions are chosen where analyte absorption is not expected. </p>
<ul>
  <li>
    <strong>diff</strong> excludes the overlapping region.
  </li>
  <li>
    <strong>mean</strong> uses the mean absorbance (default).
  </li>
  <li>
    <strong>segment 1</strong> uses the background region of segment 1.
  </li>
  <li>
    <strong>segment 2</strong> uses the background region of segment 2.
  </li>
</ul>

<strong>Download results</strong>
<p>Click to download the following files:</p>
<ul>
  <li>
    <strong>input_ssb_user.json</strong> For the sake of reproducibility, the file contains all the input parameters used in the computation of the baseline correction. 
    <ul>
      <li><strong>specfile:</strong> name of the file that contains the spectra.</li>
      <li><strong>samplelistfile:</strong> name of the file that contains the list of the samples computed.</li>
      <li><strong>param:</strong> list of EDF values that are used in the baseline correction.</li>
      <li><strong>whichsegment:</strong> list of the segments that are baseline-corrected.</li>
      <li><strong>specfile_reader:</strong> type of the specfile.</li>
      <li><strong>samplelistfile_reader:</strong> type of the samplelistfile.</li>
    </ul>
  </li>
  <li>
    <strong>ssb_selected.json</strong> For the sake of reproducibility, the file contains: 
    <ul>
      <li><strong>selected:</strong> the EDF parameter choices for each segment.</li>
      <li><strong>stitch:</strong> stitching method selected.</li>
      <li><strong>inputfile:</strong> name of the file that contains all the input parameters used in the computation of the baseline correction (./input_ssb_user.json).</li>
    </ul>
    
  <li>
    <strong>segm1_agg_criteria_table.csv</strong> and <strong>segm2ext_agg_criteria_table.csv</strong> contain the summary statistics of the aggregated NAF and aggregated total normalized absolute blank absorbance (if blank samples are provided). 
  </li>
  <li>
    <strong>segm1_agg_criteria.pdf</strong> and <strong>segm2ext_agg_criteria.pdf</strong> display the summary statistics of the aggregated NAF and aggregated total normalized absolute blank absorbance (if blank samples are provided). Black lines refer to the aggregated median, red dashed lines refer to aggregated mean, and gray regions represent the first and third quartile range.
  </li>
  <li>
    <strong>segm1_baseline_param.csv</strong> and <strong>segm2ext_baseline_param.csv</strong> contain the wavenumber bounds of the computed smoothing spline and the effective EDF of each baseline-corrected spectra.
  </li>
   <li>
    <strong>segm1_baseline.rds</strong> and <strong>segm2ext_baseline.rds</strong> are the baseline matrix (load with readRDS) of each segment. Each row represents a single baseline, and the sample names are provided in the rownames attribute; wavenumbers are saved in the "wavenum" attribute.
  </li>
  <li>
    <strong>segm1_baselinedb.sqlite</strong> and <strong>segm2ext_baselinedb.sqlite</strong> are SQL databases that contain baselines generated for each spectra and parameter, and additional parameters if they are estimated internally. The current implementation of the database is <a href="https://sqlite.org/" target="_blank">SQLite </a>. The user can inspect the contents of the output file using the <a href="http://sqlitebrowser.org/" target="_blank">DB Browser for SQLite </a>. 
  </li>
  <li>
    <strong>segm1_blankabs.csv</strong> and <strong>segm2ext_blankabs.csv</strong> (if blanks are provided) contain the aggregated total normalized absolute blank absorbance for each sample and EDF.
  </li>
  <li>
    <strong>segm1_naf.csv</strong> and <strong>segm2xt_naf.csv</strong> contain the NAF for each sample and EDF.
  </li>
  <li>
    <strong>segm1_spec.rds</strong> and <strong>segm2ext_spec.rds</strong> are the baseline-corrected absorbance spectra matrix (load with readRDS) of each segment. Each row represents a single spectrum, and the sample names are provided in the rownames attribute; wavenumbers are saved in the "wavenum" attribute.
  </li>
  <li>
    <strong>spectra_baselined.csv</strong> and <strong>spectra_baselined.rds</strong> are the baseline-corrected absorbance spectra matrix (load with readRDS) of the stitched segments. In the .csv file, the first column contains the wavenumber values and the spectra in each of the subsequent columns (with sample namess as column labels). In the .rds file each row represents a single spectrum and the sample names are provided in the rownames attribute; wavenumbers are saved in the "wavenum" attribute. Wavenumbers are saved in the "wavenum" attribute.
  </li>
  <li>
    <strong>spectra_plots.pdf</strong> contain the spectra plots for each sample. Different color lines refer to spectra computed with different EDF.
  </li>
</ul>
    
<h3>Tab: paramater selection</h3>  

<p>Top panel shows the spectra plots for each sample and EDF. Different color lines refer to spectra computed with different EDF. Bottom panel contains the aggregated median NAF in function of the EDF values for each segment.</p>
      